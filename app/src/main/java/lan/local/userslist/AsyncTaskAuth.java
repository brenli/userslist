package lan.local.userslist;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Spinner;
import android.widget.Toast;

class AsyncTaskAuth extends AsyncTask<String, Void, String> {
    Spinner spinner;
    Context context;


    public  AsyncTaskAuth(Spinner spinner, Context context){
        this.spinner = spinner;
        this.context = context;
    }

    @Override
    protected String doInBackground(String... param) {
        String uid =param[0];
        String pass = param[1];
        ApiService api = new Api();
        String result = api.sendAuthRequest(uid, pass);
        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        Toast toast=Toast.makeText(context,s,Toast.LENGTH_LONG);
        toast.show();
    }
}
