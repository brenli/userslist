package lan.local.userslist;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.List;

class AsyncTaskRunner extends AsyncTask<Void, Void, List<ListUser>> {
    Spinner spinner;
    Context context;
    EditText editText;

    private void clearPasswd(){
        editText.setText("");
    }

    public AsyncTaskRunner(Spinner spinner, Context context, EditText editText){
        this.spinner = spinner;
        this.context = context;
        this.editText = editText;
    }

    @Override
    protected List<ListUser> doInBackground(Void... params) {
        ApiService api = new Api();
        List<ListUser> users = api.getUsers();
        return users;
    }

    @Override
    protected void onPostExecute(List<ListUser> result){
        final List<ListUser> users = result;
        ArrayAdapter<ListUser> adapter = new ArrayAdapter<>(context,R.layout.row,R.id.textRow, users);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(context,users.get(position).getUser(),Toast.LENGTH_SHORT).show();
                clearPasswd();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                clearPasswd();
            }
        });

    }
}
