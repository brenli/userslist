package lan.local.userslist;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class ListUser {
    @NonNull
    @Override
    public String toString() {
        return this.user;
    }
    @SerializedName("User")
    @Expose
    private String user;
    @SerializedName("Uid")
    @Expose
    private String uid;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

}
