package lan.local.userslist;

import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

class Api implements ApiService{
    private List<Header> headers;

    private void disableCheckSSL(){
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };
        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {

        }
    }

    public Api() {
        this.headers = new ArrayList<>();
        this.headers.add(new AuthHeader("http", "http"));
        disableCheckSSL();
    }


    private class Urls {
        private String Users(String imei) {
            return  "https://dev.sitec24.ru/UKA_TRADE/hs/MobileClient/" + imei + "/form/users";
        }

        private String Authentification(String imei, String uid, String pass){
            return  String.format("https://dev.sitec24.ru/UKA_TRADE/hs/MobileClient/%s/authentication?uid=%s&pass=%s&copyFromDevice=false&nfc="+"",imei,uid,pass);
        }
    }

    @Override
    public String sendAuthRequest(String uid, String pass){
        URL url = null;
        String urlPath = new Urls().Authentification("11111111111",uid, pass);
        if (uid.isEmpty()&&pass.isEmpty()) throw new IllegalArgumentException();
        try{
            url = new URL(urlPath);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            for (Header header: headers){
                connection.setRequestProperty(header.key(), header.value());
            }
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);

            if (connection.getResponseCode()== 200) {
                StringBuilder stringBuilder = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }

                return stringBuilder.toString();

            }
        }catch (Exception e) {
            Log.e("MyEx",e.getMessage());
        }
        return urlPath;

    }

    @Override
    public List<ListUser> getUsers() {

        try {

            URL url = new URL(new Urls().Users("11111111111"));

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            //connection.setRequestProperty("Authorization", "Basic aHR0cDpodHRw");
            for (Header header: headers){
                connection.setRequestProperty(header.key(), header.value());
            }

            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);

            if (connection.getResponseCode() == 200) {
                StringBuilder stringBuilder = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                Gson gson = new Gson();
//                String json = "";
                String line;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }

                return gson.fromJson(stringBuilder.toString(),UsersResponse.class).getUsers().getListUsers();

            }
        }catch (Exception e) {
            Log.e("MyEx",e.getMessage());
        }
        return  null; //todo should not be null
    }
}
