package lan.local.userslist;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity{
    Spinner spinner;
    EditText pass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinner = findViewById(R.id.spiner);
        pass = findViewById(R.id.editText);

        AsyncTaskRunner task = new AsyncTaskRunner(spinner,getApplicationContext(),pass);
        task.execute();
    }

    public void onClickAuth(View view) {
        String uid = ((ListUser)spinner.getSelectedItem()).getUid();

        AsyncTaskAuth task = new AsyncTaskAuth(spinner,getApplicationContext());
        task.execute(uid, pass.getText().toString());
    }


}



