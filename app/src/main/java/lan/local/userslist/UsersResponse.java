package lan.local.userslist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class UsersResponse {

    @SerializedName("Users")
    @Expose
    private Users users;

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

}