package lan.local.userslist;

import java.util.List;

interface ApiService {
    List<ListUser> getUsers();
    String sendAuthRequest(String uid, String pass);
}
