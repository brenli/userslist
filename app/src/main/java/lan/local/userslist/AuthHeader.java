package lan.local.userslist;
import android.util.Base64;

class AuthHeader implements Header {
    private final String hash;

    public AuthHeader(String user, String password) {
        hash = Base64.encodeToString((user+":"+password).getBytes(), Base64.NO_WRAP);
    }

    public String key() {
        return "Authorization";
    }

    public String value() {
        return "Basic " + hash;
    }
}