package lan.local.userslist;

public interface Header {
    String key();
    String value();
}
